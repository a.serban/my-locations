// core components/views
import CategoriesView from "./app/components/views/Categories/mapping";

const routes = [
	{
		path: "categories",
		name: "Categories",
		component: CategoriesView,
		layout: "/"
	}
];

export default routes;
