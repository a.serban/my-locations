import * as React from "react";
import {
	BrowserRouter as Router,
	Switch,
	Route,
	Link, Redirect, HashRouter, BrowserRouter
} from "react-router-dom";
import "./App.scss";
import MainComponent from "./components/structure/MainComponent";

export default function App() {
	return (
		<BrowserRouter>
			<Switch>
				<Route path="/" component={MainComponent}/>
				<Redirect from="/" to="/categories"/>
			</Switch>
		</BrowserRouter>
	);
}

