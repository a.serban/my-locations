import {combineReducers} from 'redux';
import literalsReducer from "./literalsReducer";
import {connectRouter} from "connected-react-router";
import {toolbarReducer} from "./toolbarReducer";
import {categoriesReducer} from "./categoryReducer";

export default function createRootReducer(history: any) {
	return combineReducers(
		{
			router: connectRouter(history),
			literals: literalsReducer,
			toolbar: toolbarReducer,
			category: categoriesReducer
		},
	);
}
