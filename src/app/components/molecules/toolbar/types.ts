import {ToolbarActions} from "../../../models/ToolbarActions";

export interface ToolbarProps {
	readonly literals?: any;
	readonly toolbarActions: ToolbarActions;
}
