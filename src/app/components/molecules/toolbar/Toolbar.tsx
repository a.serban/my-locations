import React from "react";
import {ToolbarProps} from "./types";
import Button from "../../atoms/button/Button";
import {DEFAULT} from "../../atoms/button/constants";

//styles
import './Toolbar.scss';
import {cn} from "../../../utils";
const bem = cn('toolbar');

export default function Toolbar(props: ToolbarProps) {
	const {literals, toolbarActions} = props;
	return (
		<div className={bem()}>
			<h3>
				{toolbarActions.title}
			</h3>
			<div className={bem('button-container')}>
				{toolbarActions.create && <Button type={DEFAULT} label={literals['create']} onClick={toolbarActions.create}/>}
				{toolbarActions.viewDetails && <Button type={DEFAULT} label={literals['viewDetails']} onClick={toolbarActions.viewDetails}/>}
				{toolbarActions.edit && <Button type={DEFAULT} label={literals['edit']} onClick={toolbarActions.edit}/>}
				{toolbarActions.delete && <Button type={DEFAULT} label={literals['delete']} onClick={toolbarActions.delete}/>}
			</div>
		</div>
	)
}
