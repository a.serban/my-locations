import {Types} from "./constants";

export type Type = typeof Types[number];

export interface ButtonProps {
	readonly label?: string;
	readonly onClick?: () => void;
	readonly type: Type;
}
