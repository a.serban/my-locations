import * as React from "react";
import {ButtonProps} from "./types";

//styles
import "./Button.scss";
import {cn} from "../../../utils";

const bem = cn('button');

export default function Button(props: ButtonProps) {

	const {label, onClick, type} = props;


	return (
		<button onClick={onClick} className={bem('', {type})}>
			{label}
		</button>

	)
}
