//action type constants
export const LOAD_LITERALS = 'LOAD_LITERALS';

export const LOAD_CATEGORIES = 'LOAD_CATEGORIES';
export const SET_CATEGORY = 'SET_CATEGORY';

export const SET_TOOLBAR_ACTIONS = 'SET_TOOLBAR_ACTIONS';
export const SET_TOOLBAR_TITLE = 'SET_TOOLBAR_TITLE';
