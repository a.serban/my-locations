//actions
import {Category, ToolbarActions} from "../models";

export interface LoadLiteralsAction {
	type: string;
	payload: any;
}

export interface ToolbarAction {
	type: string;
	payload: ToolbarActions;
}

export interface CategoriesAction {
	type: string;
	payload: Category | Category[];
}

